FROM php:7.3-fpm

RUN apt-get update

RUN apt-get install -y libzip-dev

RUN apt-get -y install cron

RUN apt-get -y install nano

RUN docker-php-ext-install -j$(nproc) pcntl json mbstring bcmath opcache pdo pdo_mysql mysqli zip

RUN apt-get install -y libcurl4-openssl-dev

RUN docker-php-ext-install -j$(nproc) curl

RUN apt-get install -y \
  libpng-dev \
  libfreetype6-dev \
  libjpeg-dev \
  libxpm-dev

RUN docker-php-ext-configure gd \
    --with-freetype-dir=/usr/include/ \
    --with-jpeg-dir=/usr/include/ \
    --with-xpm-dir=/usr/include

RUN docker-php-ext-install -j$(nproc) gd

RUN apt-get install -y libicu-dev

RUN docker-php-ext-install -j$(nproc) intl

RUN curl -sS https://getcomposer.org/installer | \
    php -- --install-dir=/usr/bin/ --filename=composer

WORKDIR /src

EXPOSE 9000

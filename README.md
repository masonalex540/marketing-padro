
## Padro Marketing

Add your product,and marketer users will share them.

## Installation

In root of project run below command :

`$ docker-compose up --build -d`

Above commend will build docker containers.

After that check if nginx,php and mysql containers are up with below command :

`$ docker ps`

After all run below command to initiate laravel : 

`$ docker-compose exec php bash deploy.sh`

NOTE: For above command all container should be up and running.

With above command everything is ready to use !

## Test

To running test run below command : 

`$ docker-compose exec php php artisan test --testsuite=Feature`

## Api documentation

To see all endpoint documents visit following url :

http://localhost/api/documentation


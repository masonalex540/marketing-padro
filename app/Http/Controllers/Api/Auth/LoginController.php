<?php


namespace App\Http\Controllers\Api\Auth;


use App\Http\Traits\Authenticates;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Laravel\Sanctum\NewAccessToken;

class LoginController extends Controller
{
    use Authenticates;

    /**
     * Send the response after the user was authenticated.
     *
     * @param Request $request
     * @return JsonResponse
     */
    protected function sendLoginResponse(Request $request)
    {
        $token = $this->authenticated($request, $this->guard()->user());

        return responseApi(['token' => $token->plainTextToken]);

    }

    /**
     * The user has been authenticated.
     *
     * @param Request $request
     * @param User $user
     * @return NewAccessToken
     */
    protected function authenticated(Request $request, User $user): NewAccessToken
    {
        return $user->createToken($request->device_name ?? 'login-token');
    }

    /**
     * Log the user out of the application.
     *
     * @param Request $request
     * @return JsonResponse|void
     */
    public function logout(Request $request)
    {
        $this->guard()->user()->currentAccessToken()->delete();

        if ($response = $this->loggedOut($request)) {
            return $response;
        }

        return responseApi([], [], [], 200);

    }

}

<?php


namespace App\Http\Controllers\Api\Auth;


use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Routing\Controller;
use \Illuminate\Http\JsonResponse;

class RegisterController extends Controller
{
    /**
     * @var User
     */
    public $userModel;

    /**
     * RegisterController constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->userModel = $user;
    }

    /**
     * Register user
     *
     * @param RegisterRequest $request
     * @return JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        $user = $this->userModel->create($request->validated());

        return responseApi(['user' => $user]);
    }
}

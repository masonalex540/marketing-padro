<?php


namespace App\Http\Controllers\Api\Product;


use App\Http\Requests\CreateProductRequest;
use App\Http\Requests\CreateSpecialLinkRequest;
use App\Http\Requests\DeleteProductRequest;
use App\Http\Requests\ProductIndexRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Http\Resources\MarketingLinkResource;
use App\Http\Resources\ProductResource;
use App\Http\Traits\Uploader;
use App\Models\Product;
use App\Services\MarketingLink\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    use Uploader;

    /**
     * @var Product
     */
    public $productModel;

    /**
     * ProductController constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->productModel = $product;
    }

    /**
     * @param ProductIndexRequest $request
     * @return JsonResponse
     */
    public function index(ProductIndexRequest $request)
    {
        $filters = $request->validated();

        $filters['user'] = Auth::user();

        $products = $this->productModel->productList($filters, 5);

        return responseApi(ProductResource::collection($products)->response()->getData(true));
    }

    /**
     * Create product
     *
     * @param CreateProductRequest $request
     * @return JsonResponse
     */
    public function store(CreateProductRequest $request): JsonResponse
    {
        $cover = $this->upload($request->file('cover'));

        $product = $this->productModel->createProduct($request->validated(), $cover);

        return responseApi(new ProductResource($product));
    }

    /**
     * Update profile
     *
     * @param Product $product
     * @param UpdateProductRequest $request
     * @return JsonResponse
     */
    public function update(Product $product, UpdateProductRequest $request): JsonResponse
    {
        $cover = null;
        if ($request->file('cover')) {
            $cover = $this->upload($request->file('cover'));
        }

        $product = $product->updateProduct($request->validated(), $cover);

        return responseApi(new ProductResource($product));
    }

    /**
     * @param Product $product
     * @param DeleteProductRequest $request
     * @return JsonResponse
     */
    public function delete(Product $product,DeleteProductRequest $request): JsonResponse
    {
        $product->delete();

        return responseApi();
    }

    /**
     * @param CreateSpecialLinkRequest $request
     * @param Product $product
     * @param Builder $builder
     * @return JsonResponse
     * @throws \App\Exceptions\MarketingLinkException
     */
    public function getSpecialLink(CreateSpecialLinkRequest $request, Product $product, Builder $builder)
    {
        $link = $builder->product($product)->user($request->user())->generate();

        return responseApi(new MarketingLinkResource($link));
    }
}

<?php


namespace App\Http\Controllers\Api\Statistics;


use App\Http\Requests\MarketedProductsVistsRequest;
use App\Http\Requests\ProductStatisticsRequest;
use App\Http\Resources\MarketedProductsWithVisitsResource;
use App\Models\Product;
use App\Services\Statistics\ProductStatistics;
use \Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class StatisticsController
{
    /**
     * @var ProductStatistics
     */
    public $statistics;

    /**
     * StatisticsController constructor.
     * @param ProductStatistics $productStatistics
     */
    public function __construct(ProductStatistics $productStatistics)
    {
        $this->statistics = $productStatistics;
    }

    /**
     * @param Product $product
     * @param ProductStatisticsRequest $request
     * @return JsonResponse
     */
    public function productVisit(Product $product, ProductStatisticsRequest $request): JsonResponse
    {
        return responseApi([
            'visit_count' => $this->statistics->visitCountForProduct($product, $request->validated())
        ]);
    }

    /**
     * @param MarketedProductsVistsRequest $request
     * @return JsonResponse
     */
    public function marketedProductsVisits(MarketedProductsVistsRequest $request): JsonResponse
    {
        $result = $this->statistics->marketedProductsWithVisitCount(Auth::user(), $request->validated());

        return responseApi(MarketedProductsWithVisitsResource::collection($result));
    }
}

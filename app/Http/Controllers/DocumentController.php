<?php


namespace App\Http\Controllers;


class DocumentController
{
    /**
     * @OA\Info(title="PadroMarketing Api", version="0.1")
     */

    /**
     * @OAS\SecurityScheme(
     *       securityScheme="bearerAuth",
     *       type="http",
     *       scheme="bearer"
     *  )
     */

    /**
     * @OA\Post(
     *      path="/api/auth/register",
     *      operationId="resgisterUser",
     *      tags={"Auth"},
     *      summary="Register User",
     *      description="Register User",
     *   	@OA\RequestBody(
     *    		@OA\JsonContent(
     *                    type="object",
     *    				 @OA\Property(property="name",
     *                        type="string",
     *                        description="user full name",
     *                        example="test test"
     *                    ),
     *    				 @OA\Property(property="email",
     *                        type="string",
     *                        description="user email",
     *                        example="test@gmail.com"
     *                    ),
     *    				 @OA\Property(property="password",
     *                        type="string",
     *                        description="user password",
     *                        example="123456789"
     *                    ),
     *    				 @OA\Property(property="password_confirmation",
     *                        type="string",
     *                        description="user password confirmation",
     *                        example="123456789"
     *                    ),
     *    				 @OA\Property(property="type",
     *                        type="integer",
     *                        description="user type, 1 for maanger 2 for marketer",
     *                        example=1
     *                    ),
     *          ),
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error",
     *          @OA\JsonContent()
     *       ),
     * )
     *
     *
     */


    /**
     * @OA\Post(
     *      path="/api/auth/login",
     *      operationId="loginUser",
     *      tags={"Auth"},
     *      summary="Login User",
     *      description="Login User",
     *   	@OA\RequestBody(
     *    		@OA\JsonContent(
     *                    type="object",
     *    				 @OA\Property(property="email",
     *                        type="string",
     *                        description="user email"
     *                    ),
     *    				 @OA\Property(property="password",
     *                        type="string",
     *                        description="user password"
     *                    )
     *          ),
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error",
     *          @OA\JsonContent()
     *       ),
     * )
     *
     */

    /**
     * @OA\Get(
     *      path="/api/auth/logout",
     *      operationId="logoutUser",
     *      tags={"Auth"},
     *      summary="Logout User",
     *      description="Logout User",
     *      security={{"bearerAuth":{}}},
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       )
     * )
     *
     */


    /**
     * @OA\Post(
     *      path="/api/product/create",
     *      operationId="UserCreateProduct",
     *      tags={"Product"},
     *      summary="Create Product",
     *      description="Manager users can create product with this api.",
     *      security={{"bearerAuth":{}}},
     *   	@OA\RequestBody(
     *    		@OA\MediaType(
     *               mediaType="multipart/form-data",
     *               @OA\Schema(
     *                    required={"title","cover","description","url"},
     *    				 @OA\Property(property="title",
     *                        type="string",
     *                        description="title for product",
     *                        example="test",
     *                    ),
     *    				 @OA\Property(property="description",
     *                        type="string",
     *                        description="description for product",
     *                        example="lorem"
     *                    ),
     *    				 @OA\Property(property="url",
     *                        type="string",
     *                        description="product url",
     *                        example="https://google.com"
     *                    ),
     *    				 @OA\Property(property="cover",
     *                        type="file",
     *                        description="product image"
     *                    )
     *               ),
     *          ),
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error",
     *          @OA\JsonContent()
     *       ),
     * )
     *
     *
     */


    /**
     * @OA\Post(
     *      path="/api/product/update/{product}",
     *      operationId="UserUpdateProduct",
     *      tags={"Product"},
     *      summary="Update Product",
     *      description="Manager users can update product with this api.",
     *      security={{"bearerAuth":{}}},
     *   	@OA\Parameter(
     *         name="product",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         explode=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *   	@OA\RequestBody(
     *    		@OA\MediaType(
     *               mediaType="multipart/form-data",
     *               @OA\Schema(
     *    				 @OA\Property(property="title",
     *                        type="string",
     *                        description="title for product",
     *                        example="test",
     *                    ),
     *    				 @OA\Property(property="description",
     *                        type="string",
     *                        description="description for product",
     *                        example="lorem"
     *                    ),
     *    				 @OA\Property(property="url",
     *                        type="string",
     *                        description="product url",
     *                        example="https://google.com"
     *                    ),
     *    				 @OA\Property(property="cover",
     *                        type="file",
     *                        description="product image"
     *                    )
     *               ),
     *          ),
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       ),
     *     @OA\Response(
     *          response=422,
     *          description="validation error",
     *          @OA\JsonContent()
     *       ),
     * )
     *
     *
     */

    /**
     * @OA\Delete (
     *      path="/api/product/delete/{product}",
     *      operationId="deleteProduct",
     *      tags={"Product"},
     *      summary="Delete product",
     *      description="Manager user can delete the product",
     *      security={{"bearerAuth":{}}},
     *   	@OA\Parameter(
     *         name="product",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         explode=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       )
     * )
     *
     */

    /**
     * @OA\Get (
     *      path="/api/product/special-link/{product}",
     *      operationId="generateSpecialLink",
     *      tags={"Product"},
     *      summary="generate special link product",
     *      description="marketer user can get his own special link for marketing.",
     *      security={{"bearerAuth":{}}},
     *   	@OA\Parameter(
     *         name="product",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         explode=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       )
     * )
     *
     */

    /**
     * @OA\Get (
     *      path="/api/product/index",
     *      operationId="productIndex",
     *      tags={"Product"},
     *      summary="Product index",
     *      description="users can see list of their products",
     *      security={{"bearerAuth":{}}},
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       )
     * )
     *
     */


    /**
     * @OA\Get (
     *      path="/api/statistics/product/{product}",
     *      operationId="ProductStatistics",
     *      tags={"Statistics"},
     *      summary="Product Statistics",
     *      description="manager user can see product visits count and can filter by date range and marketer user",
     *      security={{"bearerAuth":{}}},
     *   	@OA\Parameter(
     *         name="product",
     *         in="path",
     *         description="product ID",
     *         required=true,
     *         explode=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *   	@OA\Parameter(
     *         name="from_date",
     *         in="query",
     *         description="started date filter",
     *         required=false,
     *         explode=true,
     *         @OA\Schema(
     *             type="date",
     *         )
     *     ),
     *   	@OA\Parameter(
     *         name="to_date",
     *         in="query",
     *         description="ended date filter",
     *         required=false,
     *         explode=true,
     *         @OA\Schema(
     *             type="date",
     *         )
     *     ),
     *   	@OA\Parameter(
     *         name="owner",
     *         in="query",
     *         description="marketer user id",
     *         required=false,
     *         explode=true,
     *         @OA\Schema(
     *             type="integer",
     *         )
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       )
     * )
     *
     */




    /**
     * @OA\Get (
     *      path="/api/statistics/marketed/products",
     *      operationId="ProductsStatistics",
     *      tags={"Statistics"},
     *      summary="Product list with Statistics",
     *      description="marketer user can see product list with visits count and can filter by date range",
     *      security={{"bearerAuth":{}}},
     *   	@OA\Parameter(
     *         name="from_date",
     *         in="query",
     *         description="started date filter",
     *         required=false,
     *         explode=true,
     *         @OA\Schema(
     *             type="date",
     *         )
     *     ),
     *   	@OA\Parameter(
     *         name="to_date",
     *         in="query",
     *         description="ended date filter",
     *         required=false,
     *         explode=true,
     *         @OA\Schema(
     *             type="date",
     *         )
     *     ),
     *      @OA\Response(
     *          response=200,
     *          description="successful operation",
     *          @OA\JsonContent()
     *       )
     * )
     *
     */



}

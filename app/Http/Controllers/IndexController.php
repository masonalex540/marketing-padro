<?php


namespace App\Http\Controllers;


use App\Models\MarketingLink;
use App\Services\MarketingLink\VisitRecorder;

class IndexController extends \Illuminate\Routing\Controller
{
    /**
     * @param string $code
     * @param MarketingLink $marketingLink
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function view(string $code, MarketingLink $marketingLink)
    {
        $marketingLink = $marketingLink->getByCode($code);

        if (!$marketingLink->product) {
            abort(404);
        }

        return view('redirect', [
            'product' => $marketingLink->product,
            'marketing_code' => $code
        ]);
    }

    /**
     * @param string $code
     * @param MarketingLink $marketingLink
     * @param VisitRecorder $visitRecorder
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirect(string $code, MarketingLink $marketingLink, VisitRecorder $visitRecorder)
    {
        $marketingLink = $marketingLink->getByCode($code);

        if (!$marketingLink->product) {
            abort(404);
        }

        $visitRecorder->handle($marketingLink);

        return redirect($marketingLink->product->url);
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MarketedProductsWithVisitsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->product->id,
            'title' => $this->product->title,
            'description' => $this->product->description,
            'url' => $this->product->url,
            'marketing_code' => $this->code,
            'visit_count' => $this->visits_count,
            'cover' => new ImageResource($this->product->cover()),
        ];
    }
}

<?php


namespace App\Http\Traits;


use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\File as FileModel;

trait FileTrait
{
    /**
     * attach file to this Model
     *
     * @param $file_id
     * @param $type
     */
    public function attachFile($file_id, $type)
    {
        $this->files()->attach($file_id, [
            'type' => $type,
            'owner_type' => get_class($this)
        ]);
    }

    /**
     * loads attached files
     *
     * @return HasMany
     */
    public function files()
    {
        return $this->belongsToMany(FileModel::class, 'file_attachments', 'owner_id', 'file_id')
            ->wherePivot('owner_type', '=', get_class($this))->withPivot('type', 'id')->withTimestamps();
    }

    /**
     * check if file is attached to this row in this model
     * @param $file_id
     * @return int
     */
    public function isFileAttached($file_id)
    {
        return $this->files()->where("files.id", $file_id)->count();
    }

}

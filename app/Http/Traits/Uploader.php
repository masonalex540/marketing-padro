<?php


namespace App\Http\Traits;


use App\Models\File;
use App\Models\File as FileModel;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

trait Uploader
{
    /**
     * @var array
     */
    protected $image_mimes = ['jpg', 'jpeg', 'png', 'gif', 'bmp', 'svg', 'webp'];
    /**
     * @var array
     */
    protected $video_mimes = ['mp4', 'mkv'];
    /**
     * @var array
     */
    protected $audio_mimes = ['mp3', 'wav', 'ogg'];

    /**
     * @param UploadedFile $file
     * @return FileModel
     */
    public function upload(UploadedFile $file): FileModel
    {
        $saved_path = $this->saveToStorage($file);

        return $this->saveToDb($file, $saved_path);
    }

    /**
     * @param $file
     * @return mixed
     */
    public function saveToStorage($file)
    {
        $save_as = $this->nameCreator($file);
        $saved_path = Storage::disk($this->disk())->putFileAs($this->path(), $file, $save_as);

        if ($this->isImage($file)) {
            $this->saveThumbnails($file, $save_as);
        }

        return $saved_path;
    }

    /**
     * @param $file
     * @param $save_as
     */
    public function saveThumbnails($file, $save_as)
    {
        $image = Image::make($file);

        $small_image = $image->widen(370);
        $small_image->encode($image->mime());

        $medium_image = $image->widen(540);
        $medium_image->encode($image->mime());

        Storage::disk($this->disk())->put($this->path() . '/small_' . $save_as, $small_image->__toString());
        Storage::disk($this->disk())->put($this->path() . '/medium_' . $save_as, $medium_image->__toString());

    }

    /**
     * @param $file
     * @param $saved_path
     * @return mixed
     */
    public function saveToDb($file, $saved_path)
    {
        $owner = $this->owner();

        return FileModel::create([
            'name' => $file->getClientOriginalName(),
            'path' => $saved_path,
            'disk' => $this->disk(),
            'mime' => $file->guessExtension(),
            'type' => $this->getType($file),
            'owner_id' => optional($owner)->id,
            'owner_type' => $owner ? get_class($owner) : null,
        ]);
    }

    /**
     * @param $file
     * @return string
     */
    public function nameCreator($file)
    {
        return strtotime('now') . '_' . $file->getClientOriginalName();
    }

    /**
     * @param $file
     * @return bool
     */
    public function isImage($file)
    {
        return in_array($file->guessExtension(), $this->image_mimes);
    }

    /**
     * @param $file
     * @return bool
     */
    public function isVideo($file)
    {
        return in_array($file->guessExtension(), $this->video_mimes);
    }

    /**
     * @param $file
     * @return bool
     */
    public function isAudio($file)
    {
        return in_array($file->guessExtension(), $this->audio_mimes);
    }

    /**
     * @param $file
     * @return string
     */
    public function getType($file)
    {
        if ($this->isAudio($file)) {
            return FileModel::TYPE_AUDIO;
        } elseif ($this->isImage($file)) {
            return FileModel::TYPE_IMAGE;
        } elseif ($this->isVideo($file)) {
            return FileModel::TYPE_VIDEO;
        } else {
            return FileModel::TYPE_DOCUMENT;
        }
    }

    /**
     * @return string
     */
    public function path()
    {
        if (!property_exists($this, 'path')) {
            return date('Y-m-d', time());
        }

        return substr($this->path, -1) == '/' ? substr_replace($this->path, '', strrpos($this->path, '/'), 1) : $this->path;
    }

    /**
     * @return string
     */
    public function disk()
    {
        return property_exists($this, 'disk') ? $this->disk : 'public';
    }

    /**
     * @return \Illuminate\Contracts\Auth\Authenticatable|null
     */
    public function owner()
    {
        return Auth::user();
    }
}

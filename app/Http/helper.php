<?php

use \Illuminate\Http\JsonResponse;
/**
 * @param array $data
 * @param array $errors
 * @param array $messages
 * @param int $code
 * @return JsonResponse
 */
function responseApi($data = [], array $errors = [], array $messages = [], int $code = 200): JsonResponse
{
    $res = [
        'data' => $data,
        'errors' => $errors,
        'messages' => $messages,
        'code' => $code
    ];

    return response()->json($res, $code);
}


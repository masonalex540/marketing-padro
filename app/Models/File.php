<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

/**
 * @property string name
 * @property string path
 * @property string disk
 * @property string mime
 * @property string type
 * @property string small_path
 * @property string medium_path
 */
class File extends Model
{
    use HasFactory;

    const TYPE_IMAGE = "image";
    const TYPE_VIDEO = "video";
    const TYPE_AUDIO = "audio";
    const TYPE_DOCUMENT = "document";

    /**
     * @var array
     */
    protected $fillable = [
        'name',
        'path',
        'disk',
        'mime',
        'type',
        'owner_id',
        'owner_type'
    ];

    /**
     * @var array
     */
    protected $appends = ['small_path', 'medium_path'];

    /**
     * @return mixed|null
     */
    public function getSmallPathAttribute()
    {
        if ($this->type != "image") {
            return null;
        }
        return substr_replace($this->path, 'small_', strrpos($this->path, '/') + 1, 0);
    }

    /**
     * @return mixed|null
     */
    public function getMediumPathAttribute()
    {
        if ($this->type != "image") {
            return null;
        }
        return substr_replace($this->path, 'medium_', strrpos($this->path, '/') + 1, 0);
    }

    /**
     * Url for file
     *
     * @return mixed
     */
    public function url()
    {
        return Storage::disk($this->disk)->url($this->path);
    }

    /**
     * Url for small size image
     * @return mixed
     */
    public function smallUrl()
    {
        return Storage::disk($this->disk)->url($this->small_path);
    }

    /**
     * Url for medium size image
     *
     * @return mixed
     */
    public function mediumUrl()
    {
        return Storage::disk($this->disk)->url($this->medium_path);
    }
}

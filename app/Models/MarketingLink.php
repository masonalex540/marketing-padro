<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MarketingLink
 *
 * @package App\Models
 * @property int product_id
 * @property int owner_id
 * @property string code
 */
class MarketingLink extends Model
{
    use HasFactory;

    /**
     * @var string[]
     */
    protected $fillable = [
        'product_id', 'owner_id', 'code'
    ];

    /**
     * Get product relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function visits()
    {
        return $this->hasMany(ProductVisit::class, 'marketing_link_id');
    }

    /**
     * Get owner relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * @param $code
     * @return MarketingLink
     */
    public function getByCode($code): MarketingLink
    {
        return MarketingLink::where('code', $code)->firstOrFail();
    }
}

<?php

namespace App\Models;

use App\Http\Traits\FileTrait;
use App\Services\Filter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

/**
 * @property string title
 * @property string description
 * @property string url
 * @property int owner_id
 */
class Product extends Model
{
    use HasFactory, SoftDeletes, FileTrait, Filterable;

    const FILE_TYPE_COVER = 'COVER';

    /**
     * @var string[]
     */
    protected $fillable = [
        'title', 'description', 'url', 'owner_id'
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'files'
    ];


    /**
     * Owner relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    /**
     * Get cover file model
     *
     * @return mixed
     */
    public function cover()
    {
        return $this->files->where('pivot.type', self::FILE_TYPE_COVER)->first();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function visits()
    {
        return $this->hasManyThrough(
            ProductVisit::class,
            MarketingLink::class,
            'product_id',
            'marketing_link_id'
        );
    }

    /**
     * @param array $filters
     * @param null $paginate
     * @return mixed
     */
    public function productList(array $filters = [], $paginate = null)
    {
        $query = $this->apply($filters);

        return $paginate ? $query->paginate($paginate) : $query->get();
    }

    /**
     * @param array $data
     * @param File $cover
     * @return Product
     */
    public function createProduct(array $data, File $cover): Product
    {
        $data['owner_id'] = Auth::user()->id;

        $product = Product::create($data);

        $product->attachFile($cover->id, self::FILE_TYPE_COVER);

        return $product;
    }

    /**
     * @param array $data
     * @param File|null $cover
     * @return Product
     */
    public function updateProduct(array $data, File $cover = null): Product
    {
        $this->update(array_filter($data));

        if ($cover) {
            if ($this->cover()) {
                $this->cover()->pivot->update(['file_id' => $cover->id]);
            } else {
                $this->attachFile($cover->id, self::FILE_TYPE_COVER);
            }
        }

        return $this->refresh();
    }

    /**
     * @param array $filters
     * @return mixed
     */
    public function getVisitCount(array $filters = [])
    {
        return $this->visits()->apply($filters)->count();
    }
}

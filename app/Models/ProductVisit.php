<?php

namespace App\Models;

use App\Services\Filter\Filterable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductVisit
 * @package App\Models
 *
 * @property int marketing_link_id
 * @property string ip_address
 * @property string operating_system
 * @property string operating_system_version
 * @property string browser
 * @property string browser_version
 */
class ProductVisit extends Model
{
    use HasFactory,Filterable;

    protected $fillable = [
        'marketing_link_id', 'ip_address', 'operating_system', 'operating_system_version', 'browser', 'browser_version'
    ];

    /**
     * Get marketing link relation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function marketingLink()
    {
        return $this->belongsTo(MarketingLink::class, 'marketing_link_id');
    }
}

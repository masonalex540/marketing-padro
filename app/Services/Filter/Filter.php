<?php


namespace App\Services\Filter;


interface Filter
{

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public static function apply($query, $value);
}

<?php


namespace App\Services\Filter;


use Illuminate\Support\Arr;

trait Filterable
{

    /**
     * @param $query
     * @param $filters
     * @return mixed
     */
    public function ScopeApply($query,$filters)
    {
        return $this->applyDecoratorsFromRequest($filters, $query);
    }

    /**
     * @param $filters
     * @param $query
     * @return mixed
     */
    private function applyDecoratorsFromRequest($filters, $query)
    {

        foreach ($filters as $filterName => $value) {
            if (is_null($value)) {
                continue;
            }

            $decorator = $this->createFilterDecorator($filterName);

            if ($this->isValidDecorator($decorator)) {
                $query = $decorator::apply($query, $value);

            }
        }

        return $query;
    }

    /**
     * @param $name
     * @return string
     */
    private function createFilterDecorator($name)
    {
        return __NAMESPACE__ . '\\' . $this->filtersNamespace() . '\\' .
            str_replace(' ', '',
                ucwords(str_replace('_', ' ', $name)));
    }

    /**
     * @param $decorator
     * @return bool
     */
    private function isValidDecorator($decorator)
    {
        return class_exists($decorator);
    }

    /**
     * @param $query
     * @param $filters
     * @return mixed
     */
    private function getResults($query, $filters)
    {
        if (isset($filters['per_page'])) {
            $data = $query->paginate($filters['per_page']);
            $data->appends(Arr::except($filters, ['per_page', 'page']));
        } else {
            $data = $query->get();
        }

        return $data;
    }

    public function filtersNamespace()
    {
        return (new \ReflectionClass($this))->getShortName();
    }

}

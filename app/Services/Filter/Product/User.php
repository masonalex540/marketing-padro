<?php


namespace App\Services\Filter\Product;


use App\Services\Filter\Filter;

class User implements Filter
{
    /**
     * @param $query
     * @param $user
     * @return mixed
     */
    public static function apply($query, $user)
    {
        if ($user->type != \App\Models\User::TYPE_PRODUCT_MANAGER){
            return $query;
        }

        return $query->where('owner_id', $user->id);
    }
}

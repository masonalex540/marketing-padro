<?php


namespace App\Services\Filter\ProductVisit;


use App\Services\Filter\Filter;

class FromDate implements Filter
{
    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public static function apply($query, $value)
    {
        return $query->whereDate('product_visits.created_at', '>=', $value);
    }
}

<?php


namespace App\Services\Filter\ProductVisit;


use App\Services\Filter\Filter;

class Owner implements Filter
{

    public static function apply($query, $value)
    {
        return $query->whereHas('marketingLink', function ($q) use ($value) {
            $q->where('owner_id', $value);
        });
    }
}

<?php


namespace App\Services\MarketingLink;


use App\Exceptions\MarketingLinkException;
use App\Models\MarketingLink;
use App\Models\Product;
use App\Models\User;

class Builder
{
    /**
     * @var $user
     */
    private $user;

    /**
     * @var $product
     */
    private $product;

    /**
     * @var KeyGenerator
     */
    private $keyGenerator;

    /**
     * Builder constructor.
     */
    public function __construct()
    {
        $this->keyGenerator = new KeyGenerator();
    }

    /**
     * @param User $user
     * @return $this
     */
    public function user(User $user): Builder
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function product(Product $product): Builder
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Generates link for marketer user and saves to db
     *
     * @return MarketingLink|null
     * @throws MarketingLinkException
     */
    public function generate(): MarketingLink
    {
        if (!$this->user) {
            throw new MarketingLinkException('user not set');
        }
        if (!$this->product) {
            throw new MarketingLinkException('product not set');
        }

        if ($link = $this->getUserProductLink()) {
            return $link;
        }

        return $this->createLink();
    }

    /**
     * Saves to database
     *
     * @return MarketingLink
     */
    protected function createLink()
    {
        return MarketingLink::create([
            'product_id' => $this->product->id,
            'owner_id' => $this->user->id,
            'code' => $this->keyGenerator->generate()
        ]);
    }

    /**s
     * returns product link for given user and product
     *
     * @return MarketingLink|null
     */
    protected function getUserProductLink()
    {
        return MarketingLink::where('product_id', $this->product->id)
            ->where('owner_id', $this->user->id)->first();
    }

}

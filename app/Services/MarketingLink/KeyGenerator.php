<?php


namespace App\Services\MarketingLink;


use App\Models\MarketingLink;

class KeyGenerator
{
    /**
     * @var int
     */
    public $length;

    /**
     * KeyGenerator constructor.
     * @param int $length
     */
    public function __construct(int $length = 5)
    {
        $this->length = $length;
    }

    /**
     * @return string
     */
    public function generate()
    {
        do {
            $key = $this->random();

        } while (MarketingLink::where('code', $key)->exists());

        return $key;
    }

    /**
     * @return string
     */
    protected function random()
    {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        $randomChars = '';
        for ($i = 0; $i < $this->length; $i++) {
            $randomChars .= $characters[rand(1, 51)];
        }

        return $randomChars;
    }
}

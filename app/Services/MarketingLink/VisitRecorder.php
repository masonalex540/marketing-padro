<?php


namespace App\Services\MarketingLink;


use App\Models\MarketingLink;
use App\Models\ProductVisit;
use BrowserDetect;

class VisitRecorder
{
    public function handle(MarketingLink $marketingLink)
    {
        ProductVisit::create([
            'marketing_link_id' => $marketingLink->id,
            'ip_address' => request()->ip(),
            'browser' => BrowserDetect::browserFamily(),
            'browser_version' => BrowserDetect::browserVersion(),
            'operating_system' => BrowserDetect::platformFamily(),
            'operating_system_version' => BrowserDetect::platformVersion(),
        ]);
    }
}

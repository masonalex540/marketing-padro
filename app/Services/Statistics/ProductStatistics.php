<?php


namespace App\Services\Statistics;


use App\Models\Product;
use App\Models\User;

class ProductStatistics
{
    /**
     * @param Product $product
     * @param array $filters
     * @return mixed
     */
    public function visitCountForProduct(Product $product, array $filters = [])
    {
        return $product->getVisitCount($filters);
    }

    /**
     * @param User $user
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function marketedProductsWithVisitCount(User $user, array $filters = [])
    {
        return $user->marketingLinks()
            ->with('product')
            ->withCount(['visits' => function ($q) use ($filters) {
                if (isset($filters['from_date'])) {
                    $q->whereDate('product_visits.created_at', '>=', $filters['from_date']);
                }
                if (isset($filters['to_date'])) {
                    $q->whereDate('product_visits.created_at', '<=', $filters['to_date']);
                }
            }])->get();
    }
}

<?php

namespace Database\Seeders;

use App\Models\MarketingLink;
use App\Models\Product;
use App\Models\User;
use App\Services\MarketingLink\KeyGenerator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductVisitTestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $manager = User::create([
            'name' => 'test-manager',
            'email' => 'manager@manager.com',
            'password' => '123456789',
            'type' => User::TYPE_PRODUCT_MANAGER
        ]);

        $marketer = User::create([
            'name' => 'test-marketer',
            'email' => 'marketer@marketer.com',
            'password' => '123456789',
            'type' => User::TYPE_PRODUCT_MARKETER
        ]);

        $product = Product::create([
            'title' => 'test',
            'description' => 'test',
            'url' => 'http://google.com',
            'owner_id' => $manager->id
        ]);

        $marketing_link = MarketingLink::create([
            'product_id' => $product->id,
            'owner_id' => $marketer->id,
            'code' =>  (new KeyGenerator())->generate(),
        ]);
    }
}

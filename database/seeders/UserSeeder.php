<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Manger',
                'email' => 'manager@padro.com',
                'password' => Hash::make('123456789'),
                'type' => User::TYPE_PRODUCT_MANAGER
            ],
            [
                'name' => 'Marketer',
                'email' => 'marketer@padro.com',
                'password' => Hash::make('123456789'),
                'type' => User::TYPE_PRODUCT_MARKETER
            ]
        ]);
    }
}

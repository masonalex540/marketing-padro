<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {

    Route::group(['namespace' => 'Auth', 'prefix' => 'auth'], function () {
        Route::post('/register', 'RegisterController@register');
        Route::post('/login', 'LoginController@login');
        Route::get('/logout', 'LoginController@logout')->middleware('auth:sanctum');
    });

    Route::group(['namespace' => 'Product', 'prefix' => 'product', 'middleware' => 'auth:sanctum'], function () {
        Route::get('/index', 'ProductController@index');
        Route::post('/create', 'ProductController@store');
        Route::post('/update/{product}', 'ProductController@update');
        Route::delete('/delete/{product}', 'ProductController@delete');
        Route::get('/special-link/{product}', 'ProductController@getSpecialLink');
    });

    Route::group(['namespace' => 'Statistics', 'prefix' => 'statistics', 'middleware' => 'auth:sanctum'], function () {
        Route::get('/product/{deletable_product}', 'StatisticsController@productVisit');
        Route::get('/marketed/products', 'StatisticsController@marketedProductsVisits');
    });
});

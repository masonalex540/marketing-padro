<?php

namespace Tests\Feature;

use App\Models\MarketingLink;
use Database\Seeders\ProductVisitTestSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductVisitRedirectTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        $this->seed(ProductVisitTestSeeder::class);

        $marketing = MarketingLink::first();

        $visit_count_before = $marketing->visits()->count();

        $response = $this->get(route('product.redirect', $marketing->code));

        $visit_count_after = $marketing->visits()->count();

        $this->assertGreaterThan($visit_count_before, $visit_count_after);

        $response->assertStatus(302);
    }
}

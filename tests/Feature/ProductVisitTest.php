<?php

namespace Tests\Feature;

use App\Models\MarketingLink;
use Database\Seeders\ProductVisitTestSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductVisitTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A test that visits product and submit count
     *
     * @return void
     */
    public function test_visit_product()
    {
        $this->seed(ProductVisitTestSeeder::class);

        $marketing = MarketingLink::first();

        $response = $this->get(route('product.show', $marketing->code));

        $response->assertStatus(200);
    }
}
